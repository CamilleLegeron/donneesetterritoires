locals {
  dns_zone_incubateur = "incubateur.anct.gouv.fr"
  grist_anct_env = {
    GRIST_WIDGET_LIST_URL          = "https://betagouv.github.io/grist-custom-widgets-fr-admin/widget-list.json"
    GRIST_ALLOWED_HOSTS            = "datahub.incubateur.tech,localhost"
    ALLOWED_WEBHOOK_DOMAINS        = "gouv.fr"
    GRIST_MAX_UPLOAD_ATTACHMENT_MB = "10"
    GRIST_MAX_UPLOAD_IMPORT_MB     = "100"
  }
}

module "addok" {
  source = "./tools/addok"

  kubeconfig   = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  hostname     = "addok.${var.dev_base_domain}"
  project_slug = var.old_project_slug
}

module "grist_beta" {
  source                  = "./tools/grist"
  default_email           = var.tools_grist_beta_default_email
  monitoring_org_id       = random_string.production_secret_org_id.result
  oauth_client_id         = var.tools_grist_beta_oauth_client_id
  oauth_client_secret     = var.tools_grist_beta_oauth_client_secret
  oauth_domain            = var.tools_grist_beta_oauth_domain
  domain                  = "grist.incubateur.net"
  project_slug            = "${var.project_slug}-grist-beta"
  scaleway_project_config = var.scaleway_project_config
  grist_limits_memory_mb  = 8 * 1024
  grist_persistence_size  = "20Gi"
  backup_schedule         = "0 3 * * *"
  cors_allow_origin       = "^https://.*\\\\.anct\\\\.gouv\\\\.fr$"
  image_repository        = "gristlabs/grist"
  image_tag               = "1.1.9"
  database_volume_size    = "15Gi"

  providers = {
    helm       = helm.production
    kubernetes = kubernetes.production
    scaleway   = scaleway.project
  }
}
resource "scaleway_object_bucket" "beta_grist_backups_legacy" {
  provider = scaleway.project
  name     = "grist-beta-backups"
}

module "grist_dev" {
  source                  = "./tools/grist_ha"
  scaleway_project_config = var.scaleway_project_config
  domain                  = "grist.dev.${local.dns_zone_incubateur}"
  oauth_domain            = var.development_tools_grist_oauth_domain
  oauth_client_id         = var.development_tools_grist_oauth_client_id
  oauth_client_secret     = var.development_tools_grist_oauth_client_secret
  default_email           = var.development_tools_grist_default_email
  kubeconfig              = local.kubeconfig_development
  project_slug            = "${var.project_slug}-grist-development"
  override_namespace      = "grist"
  generate_kubeconfigs    = ["florent-fayolle", "vincent-viers"]
  grist_extra_env         = local.grist_anct_env
  cors_allow_origin       = "^https://.*(.anct.gouv.fr|incubateur.tech)$"
  image_repository        = "gristlabs/grist@sha256"
  image_tag               = "18e133152ec381157889b0a6d77c6973e337760d557c1ee229b99634b4d4a809"

  grist_doc_wk_limits_memory_mb  = 6 * 1024
  grist_doc_wk_replicas          = 2
  grist_doc_wk_requests_cpu_m    = 500
  grist_home_wk_limits_memory_mb = 1 * 1024
  grist_home_wk_replicas         = 1
  grist_home_wk_requests_cpu_m   = 200

  monitoring_org_id = random_string.development_secret_org_id.result
  providers = {
    kubernetes = kubernetes.development
    helm       = helm.development
    scaleway   = scaleway.project
  }
}
resource "scaleway_object_bucket" "grist_dev_legacy_snapshots" {
  provider = scaleway.development
  name     = "anct-dev-grist-snapshots"
  region   = "fr-par"
  versioning {
    enabled = true
  }
}

module "grist_prod" {
  source                  = "./tools/grist"
  scaleway_project_config = var.scaleway_project_config
  domain                  = "grist.${local.dns_zone_incubateur}"
  oauth_client_id         = var.production_tools_grist_oauth_client_id
  oauth_client_secret     = var.production_tools_grist_oauth_client_secret
  oauth_domain            = var.production_tools_grist_oauth_domain
  default_email           = var.production_tools_grist_default_email
  kubeconfig              = local.kubeconfig_production
  project_slug            = "${var.project_slug}-grist-production"
  override_namespace      = "grist"
  grist_limits_memory_mb  = 12 * 1024
  grist_persistence_size  = "40Gi"
  backup_schedule         = "0 3 * * *"
  generate_kubeconfigs    = ["florent-fayolle", "vincent-viers"]
  grist_extra_env         = local.grist_anct_env
  cors_allow_origin       = "^https://.*(.anct.gouv.fr|incubateur.tech)$"
  image_repository        = "gristlabs/grist"
  image_tag               = "1.1.9"
  database_volume_size    = "10Gi"

  monitoring_org_id = random_string.production_secret_org_id.result
  providers = {
    helm       = helm.production
    kubernetes = kubernetes.production
    scaleway   = scaleway.project
  }
}
