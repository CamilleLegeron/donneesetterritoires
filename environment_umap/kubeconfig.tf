module "kubeconfig_yohan_boniface" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "1.0.0"

  filename               = "donnees-yohan-boniface-${var.base_domain}.yml"
  namespace              = module.namespace.namespace
  username               = "yohan-boniface"
  project_name           = var.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}
