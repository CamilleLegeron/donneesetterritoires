#!/bin/bash

release=$(curl -s https://api.github.com/repos/gristlabs/grist-core/releases/latest | grep -Po '"tag_name": "v*\K[^"]*')
echo "Detected release $release"
echo "Updating grist.tf"
sed -i 's/\(image_tag\s*= \)".*"/\1"'$release'"/' ./tools/*/grist.tf
echo "Indexing the changes (you can discard the upgrades of some instances if you want)"
git add -p

echo "Now run 'git commit' and 'git push' to commit the changes"
