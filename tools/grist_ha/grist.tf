locals {
  grist_release_name = "grist"
  grist_typeorm_config = indent(2, <<-EOT
      TYPEORM_TYPE: postgres
      TYPEORM_DATABASE:
        secretKeyRef:
          key: dbname
          name: "${module.postgresql.secret-name}"
      TYPEORM_HOST:
        secretKeyRef:
          key: host
          name: "${module.postgresql.secret-name}"
      TYPEORM_USERNAME:
        secretKeyRef:
          key: user
          name: "${module.postgresql.secret-name}"
      TYPEORM_PASSWORD:
        secretKeyRef:
          key: password
          name: "${module.postgresql.secret-name}"
      TYPEORM_EXTRA: '{"ssl": true, "extra": {"ssl": {"rejectUnauthorized": false}}}'
      EOT
  )
}

resource "scaleway_object_bucket" "grist_backups" {
  name = "${var.project_slug}-backups"
}

resource "scaleway_object_bucket" "grist_snapshots" {
  name = "${var.project_slug}-snapshots"
  versioning {
    enabled = true
  }
}

resource "random_password" "redis_password" {
  length  = 32
  special = false
}

module "redis" {
  source        = "gitlab.com/vigigloo/tools-k8s/redis"
  version       = "0.1.1"
  namespace     = module.namespace.namespace
  chart_name    = "redis"
  chart_version = "17.4.2"

  redis_password = resource.random_password.redis_password.result
  redis_replicas = 0
}

module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "2.0.2"
  max_cpu_requests  = local.namespace_requests_cpu
  max_memory_limits = local.namespace_limits_memory
  namespace         = var.override_namespace != null ? var.override_namespace : var.project_slug
  project_name      = "Grist"
  project_slug      = var.override_namespace != null ? var.override_namespace : var.project_slug

  default_container_cpu_requests  = "200m"
  default_container_memory_limits = "128Mi"
}

resource "random_password" "auth_secret" {
  length  = 64
  special = false
}

resource "helm_release" "grist" {
  repository = "https://gitlab.com/api/v4/projects/38774220/packages/helm/stable"
  chart      = "grist"
  version    = "5.0.0"
  name       = local.grist_release_name
  namespace  = "grist"
  values = [
    <<-EOT
    image:
      repository: ${var.image_repository}
      tag: ${var.image_tag}

    commonEnvVars: &commonEnvVars
      APP_HOME_URL: https://${var.domain}
      GRIST_ORG_IN_PATH: "true"
      GRIST_DEFAULT_EMAIL: "${var.default_email}"
      GRIST_SANDBOX_FLAVOR: gvisor
      APP_STATIC_INCLUDE_CUSTOM_CSS: true
      GRIST_DEFAULT_LOCALE: fr
      GRIST_HELP_CENTER: https://outline.incubateur.anct.gouv.fr/doc/documentation-grist-YPWlYTHa8j
      GRIST_ANON_PLAYGROUND: false
      PERMITTED_CUSTOM_WIDGETS: "calendar"
      GRIST_PROMCLIENT_PORT: "9102"
      GRIST_HIDE_UI_ELEMENTS: "billing,sendToDrive"
      ${length(var.grist_extra_env) > 0 ? indent(2, yamlencode(var.grist_extra_env)) : ""}

      GRIST_ALLOWED_HOSTS: ${var.domain}
      GRIST_SINGLE_PORT: 0
      GRIST_MANAGED_WORKERS: "true"

      ${local.grist_typeorm_config}
      REDIS_URL: redis://default:${resource.random_password.redis_password.result}@${module.redis.hostname}

      GRIST_OIDC_IDP_ISSUER: "https://${var.oauth_domain}/.well-known/openid-configuration"
      GRIST_OIDC_IDP_CLIENT_ID: "${var.oauth_client_id}"
      GRIST_OIDC_IDP_CLIENT_SECRET: "${var.oauth_client_secret}"
      GRIST_OIDC_IDP_SCOPES: "openid email profile organization"

    commonPodAnnotations: &commonPodAnnotations
      monitoring-org-id: ${var.monitoring_org_id}
      prometheus.io/scrape: "true"
      prometheus.io/path: "/"
      prometheus.io/port: "9102"

    mountFiles:
      - path: "/grist/static/ui-icons/Logo/logo_anct_2022.svg"
        content: ${filebase64("${path.module}/logo_anct_2022.svg")}
      - path: "/grist/static/custom.css"
        content: ${filebase64("${path.module}/custom.css")}

    docWorker:
      replicas: ${var.grist_doc_wk_replicas}
      envVars:
        <<: *commonEnvVars

        GRIST_SERVERS: docs

        GRIST_DOCS_MINIO_BUCKET: ${scaleway_object_bucket.grist_snapshots.name}
        GRIST_DOCS_MINIO_ENDPOINT: s3.fr-par.scw.cloud
        GRIST_DOCS_MINIO_BUCKET_REGION: fr-par
        GRIST_DOCS_MINIO_ACCESS_KEY: ${var.scaleway_project_config.access_key}
        GRIST_DOCS_MINIO_SECRET_KEY: ${var.scaleway_project_config.secret_key}

      podAnnotations:
        <<: *commonPodAnnotations

      resources:
        limits:
          memory: ${local.grist_doc_wk_container_limits_memory}
        requests:
          cpu: ${local.grist_doc_wk_container_requests_cpu}

    homeWorker:
      replicas: ${var.grist_home_wk_replicas}
      envVars:
        <<: *commonEnvVars

        GRIST_SERVERS: home,static

      podAnnotations:
        <<: *commonPodAnnotations

      resources:
        limits:
          memory: ${local.grist_home_wk_container_limits_memory}
        requests:
          cpu: ${local.grist_home_wk_container_requests_cpu}

    ingress:
      enabled: true
      host: ${var.domain}
      annotations:
        acme.cert-manager.io/http01-edit-in-place: "true"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/ingress.class: haproxy
        haproxy.org/cors-enable: "true"
        haproxy.org/cors-allow-origin: "${var.cors_allow_origin}"
        haproxy.org/cors-allow-methods: "GET"
        haproxy.org/cors-allow-headers: "*"
    EOT
  ]
}

moved {
  from = module.grist_namespace
  to   = module.namespace
}
