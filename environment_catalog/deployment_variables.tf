module "configure_catalog_repository_for_deployment" {
  source  = "gitlab.com/vigigloo/tf-modules/gitlabk8svariables"
  version = "0.1.0"

  repositories = [var.project_id]
  scope        = var.gitlab_environment_scope

  cluster_user           = module.namespace.user
  cluster_token          = module.namespace.token
  cluster_namespace      = module.namespace.namespace
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = base64decode(var.kubeconfig.cluster_ca_certificate)

  base-domain = var.base_domain
}

resource "gitlab_project_variable" "helm_values" {
  project           = var.project_id
  environment_scope = var.gitlab_environment_scope
  variable_type     = "file"

  key   = "HELM_UPGRADE_VALUES"
  value = <<-EOT
  ingress:
    enabled: true
    annotations:
      kubernetes.io/ingress.class: haproxy
      acme.cert-manager.io/http01-edit-in-place: "true"
    hosts:
      - host: ${var.base_domain}
        tls: true
  podAnnotations:
    monitoring-org-id: ${var.monitoring_org_id}
  resources:
    requests:
      cpu: 100m
    limits:
      memory: 1Gi
  service:
    targetPort: 3000
  EOT
}
