variable "base_domain" {
  type = string
}

variable "project_id" {
  type = string
}

variable "project_slug" {
  type    = string
  default = "catalogue-indicateurs"
}

variable "project_name" {
  type    = string
  default = "Catalogue Indicateurs"
}

variable "gitlab_environment_scope" {
  type = string
}

variable "namespace" {
  type = string
}

variable "namespace_quota_max_cpu_requests" {
  type    = string
  default = "2"
}

variable "namespace_quota_max_memory_limits" {
  type    = string
  default = "12Gi"
}

variable "monitoring_org_id" {
  type = string
}

variable "kubeconfig" {
  type = object({
    host                   = string
    token                  = string
    cluster_ca_certificate = string
  })
}
